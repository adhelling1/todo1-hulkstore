package com.todo1.hulk.store.service.producto;

import java.util.List;

import com.todo1.hulk.store.model.Compra;
import com.todo1.hulk.store.model.Producto;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
public interface IProductoFindService {
	
	public List<Producto> getAll();
	public Producto findById(long idProducto);
	public List<Producto> findByEstado(String estado);
	public Producto findByNombre(String nombre);
	public Producto findByCodigo(String codigo);
	public List<Producto> findByValidos(Compra compra);
}
