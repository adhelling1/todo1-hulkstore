package com.todo1.hulk.store.enums;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
public enum EnumEstado {
	ACTIVO,
	BAJA;
}
