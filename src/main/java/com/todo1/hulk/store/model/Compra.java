package com.todo1.hulk.store.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
@Entity
@Getter
@Setter
public class Compra extends Entidad implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "nombreCompra", length = 20, nullable = false)
	private String nombreCompra;

	@NotNull
	@Min(value = 0, message = "El precio mínimo es 0")
	private double precioTotal;

	@OneToMany(mappedBy = "compra", cascade = CascadeType.ALL)
	private Collection<CompraProducto> compraProductos;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="id_empleado", nullable=false)
	private Empleado empleado;

	
	public void addProducto(Producto p, int cantidad) {
		if(this.getCompraProductos() == null) {
			this.setCompraProductos(new ArrayList<>());
		}
		this.compraProductos.add(new CompraProducto(cantidad, p, this));
	}

	
}
