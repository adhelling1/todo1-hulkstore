package com.todo1.hulk.store.validator.impl;

import java.util.ArrayList;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.todo1.hulk.store.error.ErrorCode;
import com.todo1.hulk.store.exception.HulkStoreException;
import com.todo1.hulk.store.model.Entidad;
import com.todo1.hulk.store.model.Producto;
import com.todo1.hulk.store.pojo.ProductoPOJO;
import com.todo1.hulk.store.service.producto.IProductoService;
import com.todo1.hulk.store.validator.IModelValidator;
import com.todo1.hulk.store.validator.IProductoUpdateValidator;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
@Component("productoValidator")
public class ProductoValidatorImpl implements IModelValidator, IProductoUpdateValidator {
	
	private static Logger logger = LoggerFactory.getLogger(ProductoValidatorImpl.class);
	
    @Autowired
    private IProductoService service;
	
	@Override
	public Collection<String> validate(Entidad entidad) throws HulkStoreException {
		logger.info("Validando entidad  {}", entidad.getClass());
		Collection<String> errors = new ArrayList<>();
		
		Producto prod = (Producto) entidad;
		Producto prodCodigo = service.findByCodigo(prod.getCodigo());
		if (prodCodigo != null && (prod.getId() == 0 || prod.getId() != prodCodigo.getId())) {
			errors.add(ErrorCode.CODIGO_REPETIDO);
		}

		if (!StringUtils.hasText(prod.getNombre())) {
			errors.add(ErrorCode.NOMBRE_PRODUCTO_OBLIGATORIO);
		}

		return errors;
		
	}

	@Override
	public boolean hasChanges(ProductoPOJO prod, Producto prodOriginal) {
		if(!prod.getCodigo().equals(prodOriginal.getCodigo())) {
			return true;
		}
		
		if(!prod.getNombre().equals(prodOriginal.getNombre())) {
			return true;
		}
		
		if(!prod.getDescripcion().equals(prodOriginal.getDescripcion())) {
			return true;
		}
		
		if(prod.getCantidad() != prodOriginal.getCantidad()) {
			return true;
		}
		
		return (prod.getPrecio() != prodOriginal.getPrecio());
				
	}

}
