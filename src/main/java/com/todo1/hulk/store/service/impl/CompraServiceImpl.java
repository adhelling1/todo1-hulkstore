package com.todo1.hulk.store.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.todo1.hulk.store.enums.EnumEstado;
import com.todo1.hulk.store.exception.HulkStoreException;
import com.todo1.hulk.store.model.Compra;
import com.todo1.hulk.store.model.CompraProducto;
import com.todo1.hulk.store.model.Empleado;
import com.todo1.hulk.store.model.Producto;
import com.todo1.hulk.store.pojo.ProductoCompraPOJO;
import com.todo1.hulk.store.repository.ICompraDAO;
import com.todo1.hulk.store.service.compra.ICompraService;
import com.todo1.hulk.store.service.empleado.IEmpleadoService;
import com.todo1.hulk.store.service.producto.IProductoService;
import com.todo1.hulk.store.validator.IModelValidator;

@Service
@Transactional
public class CompraServiceImpl implements ICompraService {

	@Autowired
	private ICompraDAO repoCompraDao;
	
	@Autowired
	private IEmpleadoService empleadoService;
	
	@Autowired
	private IProductoService productoService;
	
	@Autowired
	@Qualifier("compraValidator")
	private IModelValidator compraValidator;
	
	private static Logger logger = LoggerFactory.getLogger(CompraServiceImpl.class);
	
	/**
	 * Busca todas las compras
	 */
	@Override
	public List<Compra> getAll() {
		logger.info("Obteniendo todas las compras");
		return repoCompraDao.findAll();
	}
	
	/**
	 * Busca una compra poder identificador
	 */
	@Override
	public Compra findById(long id) {
		logger.info("Recuperando compra de id: {}" , id);
		Optional<Compra> compra = repoCompraDao.findById(id);
		if (compra.isPresent()) {
			return compra.get();
		}
		return null;
	}
	
	/**
	 *Elimina una compra 
	 */
	@Override
	public boolean delete(long id) throws HulkStoreException {
		logger.info("Borrando compra de id:  {}" , id);
		Optional<Compra> compra = repoCompraDao.findById(id);
		if (compra.isPresent()) {
			compra.get().setEstado(EnumEstado.BAJA.name());
						
			for(CompraProducto compraProducto : compra.get().getCompraProductos()){					
				devolverProducto(compraProducto.getProducto(), compraProducto.getCantidad());	
			}
			Compra c = repoCompraDao.save(compra.get());
			
			return c != null;
		
		}
		return false;
	}

	/**
	 * 
	 * @param producto
	 * @param cantCompra
	 * @throws HulkStoreException
	 */
	private void devolverProducto(Producto producto, int cantCompra) throws HulkStoreException {
		producto.setCantidad(producto.getCantidad() + cantCompra);
		productoService.modificar(producto);
	}
	
	/**
	 * Almacena una compra
	 */
	@Override
	public Compra guardar(Compra compra) throws HulkStoreException {
		logger.info("guardando compra .....");
		//verifico el empleado de la compra
		if(compra.getEmpleado() == null) {
			throw new HulkStoreException("El empleado es obligatorio");
		}
		
		//verifico el producto de la compra
		if(compra.getCompraProductos() == null || compra.getCompraProductos().isEmpty()) {
			throw new HulkStoreException("El producto es obligatorio");
		}

		//verifico que exista el empleado enviado
		Empleado empleado = empleadoService.findById(compra.getEmpleado().getId());
		if(empleado == null || empleado.getEstado().equals(EnumEstado.BAJA.name())) {
			throw new HulkStoreException("El empleado es obligatorio o no puede estar eliminado");
		}
		
		verificarCalcularPrecio(compra);
		
		compra.setEstado(EnumEstado.ACTIVO.name());
		Compra compraGuardada = validateSave(compra);
					
		return modificarCompraEmpleado(empleado, compraGuardada);
	}

	/**
	 * Valida y guarda la comora
	 * @param compra
	 * @return
	 * @throws HulkStoreException
	 */
	protected Compra validateSave(Compra compra) throws HulkStoreException {
		Collection<String> errors = compraValidator.validate(compra);
		if(errors == null || errors.isEmpty()) {
			return repoCompraDao.saveAndFlush(compra);
		}else {
			throw new HulkStoreException(errors.toString());
		}
	}

	/**
	 * modifica las compras del empleado
	 * @param empleado
	 * @param compraGuardada
	 * @return
	 * @throws HulkStoreException
	 */
	private Compra modificarCompraEmpleado(Empleado empleado, Compra compraGuardada) throws HulkStoreException {
		//actualizo las compras del empleado
		List<Compra> compras = empleado.getCompras();
		if(compras == null) {
			compras = new ArrayList<>();
		}
		compras.add(compraGuardada);
		empleado.setCompras(compras);
		empleadoService.modificar(empleado);
		return compraGuardada;
	}

	/**
	 * Calcula el precio total de la compra, verificando si los prodcutos que la contienen existen
	 * @param compra
	 * @throws HulkStoreException
	 */
	private void verificarCalcularPrecio(Compra compra) throws HulkStoreException {
		compra.setPrecioTotal(0.0);
		for(CompraProducto compraProducto : compra.getCompraProductos()) {
			//verifico que exista el producto enviado
			Producto p = productoService.findById(compraProducto.getProducto().getId());
			if (p == null || p.getEstado().equals(EnumEstado.BAJA.name())) {
				throw new HulkStoreException("El producto es obligatorio o no puede estar eliminado");
			}
			//verifico que pueda descontar la cantidad de productos comprados
			if(compraProducto.getCantidad() > p.getCantidad()) {
				throw new HulkStoreException("No se puede realizar la compra por falta de productos");
			}
			//Descuento a productos la cantidad de la compra
			p.setCantidad(p.getCantidad() - compraProducto.getCantidad());
			
			//Se calcula el precio el total de la compra
			compra.setPrecioTotal(compra.getPrecioTotal() + (p.getPrecio() * compraProducto.getCantidad()));
		}
		
		
	}
	@Override
	/***
	 * Busco por estado
	 */
	public List<Compra> findByEstado(String estado) {
		return repoCompraDao.findByEstado(estado);
	}
	
	@Override
	/**
	 * Agrego un producto a la compra si tengo stock
	 */
	public void agregarProducto(Compra compra, ProductoCompraPOJO productoCompraBean) throws HulkStoreException {
		if(productoCompraBean != null) {
			Producto p = productoService.findById(productoCompraBean.getIdProducto());
			if(p!=null) {
				if(p.getCantidad() < productoCompraBean.getCantidad()) {
					throw new HulkStoreException("Cantidad de productos insuficientes para el producto " + p.getNombre());
				}
				compra.addProducto(p, productoCompraBean.getCantidad());
			}
		}
		
	}
	@Override
	/**
	 * Calculo el precio de la compra
	 */
	public double getPrecioCompra(Compra compra) {
		double precio = 0.0;
		if(compra != null && compra.getCompraProductos() != null) {
			for(CompraProducto cp : compra.getCompraProductos()) {
				precio += cp.getCantidad() * cp.getProducto().getPrecio();
			}
		}
		return precio;
	}
	
	@Override
	/**
	 * Se utiliza para sacar un producto de la compra se sesion, por eso no hace falta devolver el stock
	 */
	public void elimiarProducto(Compra compra, int index) {
		if(compra != null && compra.getCompraProductos() != null && index >= 0) {
			ArrayList<CompraProducto> cp =   new ArrayList<>(compra.getCompraProductos());
			compra.getCompraProductos().remove(cp.get(index));
		}
		
	}

}
