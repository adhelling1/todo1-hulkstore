package com.todo1.hulk.store.service.producto;

import com.todo1.hulk.store.exception.HulkStoreException;
import com.todo1.hulk.store.model.Producto;
import com.todo1.hulk.store.pojo.ProductoPOJO;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
public interface IProductoService extends IProductoFindService {
	
	public boolean delete(long id);
	public Producto guardar(ProductoPOJO prod) throws HulkStoreException;
	public Producto modificar(ProductoPOJO prod) throws HulkStoreException;
	public Producto modificar(Producto prod) throws HulkStoreException;

}
