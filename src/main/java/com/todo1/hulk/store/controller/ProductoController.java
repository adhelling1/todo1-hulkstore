package com.todo1.hulk.store.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.todo1.hulk.store.enums.EnumEntityType;
import com.todo1.hulk.store.enums.EnumEstado;
import com.todo1.hulk.store.exception.HulkStoreException;
import com.todo1.hulk.store.factory.EntityFactory;
import com.todo1.hulk.store.model.Producto;
import com.todo1.hulk.store.pojo.ProductoPOJO;
import com.todo1.hulk.store.service.producto.IProductoService;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
@Controller
public class ProductoController {
	
	private static final String PRODUCTO = "producto";
	private static final String PRODUCTOS = "productos";

	private static Logger logger = LoggerFactory.getLogger(ProductoController.class);

	@Autowired
	private IProductoService service;

	@GetMapping("/productos")
	public String productos(Model model) {
		logger.info("Accediendo a controlador /productos");
		List<Producto> productos = service.findByEstado(EnumEstado.ACTIVO.name());
		String modelAndView = PRODUCTOS;
		model.addAttribute(modelAndView, productos);
		return modelAndView;
	}

	@GetMapping(path = "/borrarProducto/{id}")
	public String deleteProduct(@PathVariable(name = "id") long id) {
		logger.info("Accediendo a controlador /borrarProducto/{id}");
		service.delete(id);
		return "redirect:/" + PRODUCTOS;
	}
	
	@GetMapping(path = "/editarProducto/{id}")
	public String editarProducto(@PathVariable(name = "id") long id, Model model) {
		logger.info("Accediendo a controlador /editarProducto/{id}");
		Producto prod = service.findById(id);
		model.addAttribute(PRODUCTO,  prod);
		return "editarProducto";
	}


	@GetMapping("/nuevoProducto")
	public String nuevoProducto(Model model) {
		logger.info("Accediendo a controlador /nuevoProducto");
		model.addAttribute(PRODUCTO,  EntityFactory.getInstance().getEntidad((EnumEntityType.PRODUCTO)));
		return "nuevoProducto";
	}
	
	
	@PostMapping("/modificarProducto")
	public String modificarProducto(@ModelAttribute(PRODUCTO) ProductoPOJO prod, BindingResult result, RedirectAttributes redirectAttributes) throws HulkStoreException {
		logger.info("Accediendo a controlador /modificarProducto");
		service.modificar(prod);
		return "redirect:/" + PRODUCTOS;
	}

	@PostMapping("/guardarProducto")
	public RedirectView guardarProducto(@ModelAttribute(PRODUCTO) ProductoPOJO prod, BindingResult result, RedirectAttributes redirectAttributes) throws HulkStoreException {
		logger.info("Accediendo a controlador /guardarProducto");
		final RedirectView redirectView = new RedirectView(PRODUCTOS, true);
		
		Producto savedProd = service.guardar(prod);
		redirectAttributes.addFlashAttribute("savedProd", savedProd);
		redirectAttributes.addFlashAttribute("savedProdSuccess", true);
		
		return redirectView;
		
	}

}
