package com.todo1.hulk.store;

import static org.junit.jupiter.api.Assertions.assertTrue;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.todo1.hulk.store.enums.EnumEntityType;
import com.todo1.hulk.store.exception.HulkStoreException;
import com.todo1.hulk.store.factory.EntityFactory;
import com.todo1.hulk.store.model.Rol;
import com.todo1.hulk.store.service.rol.IRolService;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
@SpringBootTest
@Transactional
public class RolTest {
	
	private static Logger logger = LoggerFactory.getLogger(RolTest.class);
	
	@Autowired
	private IRolService service;
	
	@Test
	public void crearRol() {
		int count = service.getAll().size();
		Rol rol =   (Rol) EntityFactory.getInstance().getEntidad((EnumEntityType.ROL));
		rol.setNombre("ROL_TEST");
		try {
			service.guardar(rol);
		} catch (HulkStoreException e) {
			e.printStackTrace();
			logger.info("HulkStoreException en test crearRol: {}", e.getMessage());
			assertTrue(false);
		}
		count++;
		assertTrue(service.getAll().size() == count);
	}

}
