package com.todo1.hulk.store;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.todo1.hulk.store.enums.EnumEntityType;
import com.todo1.hulk.store.enums.EnumEstado;
import com.todo1.hulk.store.exception.HulkStoreException;
import com.todo1.hulk.store.factory.EntityFactory;
import com.todo1.hulk.store.model.Compra;
import com.todo1.hulk.store.model.CompraProducto;
import com.todo1.hulk.store.model.Empleado;
import com.todo1.hulk.store.model.Producto;
import com.todo1.hulk.store.pojo.ProductoPOJO;
import com.todo1.hulk.store.service.compra.ICompraService;
import com.todo1.hulk.store.service.empleado.IEmpleadoService;
import com.todo1.hulk.store.service.producto.IProductoService;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
@SpringBootTest
@Transactional
public class CompraTest {
	
	@Autowired
	private IEmpleadoService serviceEmpleado;
	
	@Autowired
	private IProductoService serviceProducto;
	
	@Autowired
	private ICompraService serviceCompra;
	
	private static Logger logger = LoggerFactory.getLogger(CompraTest.class);
	
	@Test
	public void crearCompra() throws HulkStoreException {
		logger.info("Arrancando test crear compra");
		int countCompraPre = serviceCompra.getAll().size();
		int countCompraPost = 0;
		Compra compra = (Compra) EntityFactory.getInstance().getEntidad((EnumEntityType.COMPRA));
		compra.setEstado(EnumEstado.ACTIVO.name());
		compra.setFechaAlta(new Date());
		compra.setNombreCompra("Compra 1");
		ProductoPOJO producto1 = new ProductoPOJO("p1", "la descripcion de p1", "1", 10F, 100);
		ProductoPOJO producto2 = new ProductoPOJO("p2", "la descripcion de p2", "2", 20F, 100);
		Producto prodCompra1 = serviceProducto.guardar(producto1);
		Producto prodCompra2 = serviceProducto.guardar(producto2);
		Empleado empleado = new  Empleado("Alejandro", "Helling", "adhelling48", "123", EnumEstado.ACTIVO.name(), new Date());
		serviceEmpleado.guardar(empleado, false);
		
		Collection<CompraProducto> compraProductos = new ArrayList<>();
		compraProductos.add(new CompraProducto(2, prodCompra1, compra));
		compraProductos.add(new CompraProducto(1, prodCompra2, compra));
		
		compra.setCompraProductos(compraProductos);
		compra.setEmpleado(empleado);
		
		serviceCompra.guardar(compra);
		countCompraPost = serviceCompra.getAll().size();
		
		assertTrue(countCompraPre + 1 == countCompraPost);
		logger.info("finalizando test crear compra");
			
	}

}
