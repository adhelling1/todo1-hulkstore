package com.todo1.hulk.store.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
public class HulkStoreException extends Exception {

	private static final long serialVersionUID = -1306161743322077269L;
	
	private static final Logger logger = LoggerFactory.getLogger(HulkStoreException.class);

	public HulkStoreException(String msj) {
		super(msj);
		logger.error("Creando excepcion {}", msj);
	}
}
