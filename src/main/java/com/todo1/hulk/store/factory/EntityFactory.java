package com.todo1.hulk.store.factory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.todo1.hulk.store.enums.EnumEntityType;
import com.todo1.hulk.store.model.Compra;
import com.todo1.hulk.store.model.Empleado;
import com.todo1.hulk.store.model.Entidad;
import com.todo1.hulk.store.model.Producto;
import com.todo1.hulk.store.model.Rol;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
public class EntityFactory {

	private static EntityFactory instance;
	private static Logger logger = LoggerFactory.getLogger(EntityFactory.class);

	private EntityFactory() {

	}

	public static EntityFactory getInstance() {
		if (instance == null) {
			instance = new EntityFactory();
		}
		logger.info("Obteniendo instancia de EntityFactory");
		return instance;
	}

	public Entidad getEntidad(EnumEntityType type) {
		logger.info("Creando entidad del tipo  {}", (type != null) ? type.name() : "null");
		if (type == null) {
			return null;
		}

		switch (type) {
			case PRODUCTO:
				return new Producto();
			case ROL:
				return new Rol();
			case EMPLEADO:
				return new Empleado();
			case COMPRA:
				return new Compra();
	
			default:
				logger.info("Creando entidad del tipo null");
				return null;
		}
	}

}
