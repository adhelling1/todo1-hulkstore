package com.todo1.hulk.store.service.compra;

import java.util.List;

import com.todo1.hulk.store.model.Compra;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
public interface ICompraFindService {
	
	public List<Compra> getAll();

	public Compra findById(long id);
	
	public List<Compra> findByEstado(String estado);
}
