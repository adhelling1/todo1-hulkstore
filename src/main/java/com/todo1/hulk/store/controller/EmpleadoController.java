package com.todo1.hulk.store.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.todo1.hulk.store.enums.EnumEntityType;
import com.todo1.hulk.store.enums.EnumEstado;
import com.todo1.hulk.store.exception.HulkStoreException;
import com.todo1.hulk.store.factory.EntityFactory;
import com.todo1.hulk.store.model.Empleado;
import com.todo1.hulk.store.pojo.EmpleadoModificacionPOJO;
import com.todo1.hulk.store.pojo.EmpleadoPOJO;
import com.todo1.hulk.store.service.empleado.IEmpleadoService;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
@Controller
public class EmpleadoController {
	
	private static final String EMPLEADOS = "empleados";

	private static Logger logger = LoggerFactory.getLogger(EmpleadoController.class);
	
	@Autowired
	private IEmpleadoService service;
	
	@GetMapping("/empleados")
	public String empleados(Model model) {
		logger.info("Accediendo a controlador /empleados");
		List<Empleado> empleados = service.findByEstado(EnumEstado.ACTIVO.name());
		model.addAttribute(EMPLEADOS, empleados);
		return EMPLEADOS;
	}

	@GetMapping(path = "/borrarEmpleado/{id}")
	public String deleteEmpleado(@PathVariable(name = "id") long id) throws HulkStoreException {
		logger.info("Accediendo a controlador /borrarEmpleado/{id}");
		service.delete(id);
		return "redirect:/empleados";
	}
	
	@GetMapping(path = "/editarEmpleado/{id}")
	public String editarEmpleado(@PathVariable(name = "id") long id, Model model) {
		logger.info("Accediendo a controlador /editarProducto/{id}");
		Empleado empleado= service.findById(id);
		EmpleadoModificacionPOJO emb = new EmpleadoModificacionPOJO();
		emb.setAdmin(empleado.isAdmin());
		emb.setEmpleado(empleado);
		model.addAttribute("emb",  emb);
		return "editarEmpleado";
	}
	
	@GetMapping("/nuevoEmpleado")
	public String nuevoEmpleado(Model model) {
		logger.info("Accediendo a controlador /nuevoEmpleado");
		model.addAttribute("empleado", new EmpleadoPOJO());
		return "nuevoEmpleado";
	}

	@PostMapping("/guardarEmpleado")
	public RedirectView guardarEmpleado(@ModelAttribute("empleado") EmpleadoPOJO empleadoBean, BindingResult result, RedirectAttributes redirectAttributes) throws HulkStoreException {
		logger.info("Accediendo a controlador /guardarEmpleado");
		final RedirectView redirectView = new RedirectView(EMPLEADOS, true);
		Empleado savedEmple;
		
		Empleado empleado =  (Empleado) EntityFactory.getInstance().getEntidad((EnumEntityType.EMPLEADO));
		empleado.setAll(empleadoBean.getNombre(), empleadoBean.getApellido(), empleadoBean.getAlias(), empleadoBean.getClave(), EnumEstado.ACTIVO.name(), new Date());
		savedEmple = service.guardar(empleado, empleadoBean.isAdmin());
		redirectAttributes.addFlashAttribute("savedEmpl", savedEmple);
		redirectAttributes.addFlashAttribute("savedEmplSuccess", true);
	
		return redirectView;
	}
	
	
	@PostMapping("/modificarEmpleado")
	public String modificarEmpleado(@ModelAttribute("emb") EmpleadoModificacionPOJO emplBean, BindingResult result, RedirectAttributes redirectAttributes) throws HulkStoreException {
		logger.info("Accediendo a controlador /modificarEmpleado");
		service.modificar(emplBean.getEmpleado(), emplBean.isAdmin());
		return "redirect:/empleados";
	}


}
