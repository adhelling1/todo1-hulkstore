package com.todo1.hulk.store;

import static org.junit.jupiter.api.Assertions.assertTrue;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.todo1.hulk.store.enums.EnumEstado;
import com.todo1.hulk.store.exception.HulkStoreException;
import com.todo1.hulk.store.model.Producto;
import com.todo1.hulk.store.pojo.ProductoPOJO;
import com.todo1.hulk.store.service.producto.IProductoService;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
@SpringBootTest
@Transactional
public class ProductoTest {
	
	@Autowired
	private IProductoService service;
	
	private static Logger logger = LoggerFactory.getLogger(ProductoTest.class);

	
	
	@Test
	public void crearProductos() throws HulkStoreException {
		logger.info("Arrancando test crear Productos");
		int countProdPre = service.getAll().size();
		int countProdPost = 0;
		crearProducto("p1", "la descripcion de p1", "1", 1F, 100);
		crearProducto("p2", "la descripcion de p2", "2", 1.5F, 200);
		crearProducto("p3", "la descripcion de p3", "3", 2F, 150);
		
		countProdPost = service.getAll().size();
		assertTrue( countProdPre + 3 == countProdPost);
		logger.info("Finalizando test crear Productos");		
	}
	
	private Producto crearProducto(String nombre, String descripcion, String codigo, float precio, int cantidad) throws HulkStoreException {
		ProductoPOJO p1 = new ProductoPOJO(nombre, descripcion, codigo, precio, cantidad);
		return service.guardar(p1);
	}
	
	@Test
	public void borrarProductosPorCodigo() {
		logger.info("Arrancando test borrar Productos");
		Producto p = service.findByCodigo("3");
		if(p == null) {
			try {
				crearProducto("p4", "la descripcion de p4", "4", 5F, 100);
			} catch (HulkStoreException e) {
				e.printStackTrace();
				logger.info("HulkStoreException en test borrar Productos:{}", e.getMessage());
				assertTrue(false);
				return;
			}
			p = service.findByCodigo("4");
		}
		
		if(p == null) {
			assertTrue(false);
			return;
		}else {
			service.delete(p.getId());
			
			assertTrue(service.findById(p.getId()).getEstado().equals(EnumEstado.BAJA.name()));
		}
	
		
		logger.info("Finalizando test borrar Productos");
	}

}
