package com.todo1.hulk.store.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
@Controller
public class AppController {
	
	private static Logger logger = LoggerFactory.getLogger(AppController.class);


	@GetMapping({ "/", "/login" })
	public String index() {
		logger.info("Accediendo a controlador / o /login");
		return "login";
	}

	@GetMapping("/menu")
	public String menu() {
		logger.info("Accediendo a controlador /menu");
		//TODO: 
		//si soy solo user redirigir a /user
		//si soy solo amin redirigir a /admin
		return "menu";
	}

	@GetMapping("/user")
	public String user() {
		logger.info("Accediendo a controlador /user");
		return "user";
	}

	@GetMapping("/admin")
	public String admin() {
		logger.info("Accediendo a controlador /admin");
		return "admin";
	}
}