package com.todo1.hulk.store.service.impl;

import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.todo1.hulk.store.exception.HulkStoreException;
import com.todo1.hulk.store.model.Rol;
import com.todo1.hulk.store.repository.IRolDAO;
import com.todo1.hulk.store.service.rol.IRolService;
import com.todo1.hulk.store.validator.IModelValidator;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
@Transactional
@Service
public class RolServiceImpl implements IRolService {
	
	private static Logger logger = LoggerFactory.getLogger(RolServiceImpl.class);

	@Autowired
	private IRolDAO repoRolDao;
	
	@Autowired
	private IModelValidator rolValidator;
	
	/**
	 * Almacena un rol
	 */
	@Override
	public Rol guardar(Rol rol) throws HulkStoreException {
		logger.info("Almacenando rol");
		
		Collection<String> errors = rolValidator.validate(rol);
		if(errors == null || errors.isEmpty()) {
			Rol existente = repoRolDao.findByNombre(rol.getNombre());
			if(existente == null) {
				return 	repoRolDao.saveAndFlush(rol);
			}else {
				String msj = "No se pudo crear, ya que existe uno con el mismo nombre";
				logger.info(msj);
				throw new HulkStoreException(msj);
			}
		}
		
		throw new HulkStoreException(errors.toString());
	}

	@Override
	public List<Rol> getAll() {
		return repoRolDao.findAll();
	}

}
