package com.todo1.hulk.store.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.todo1.hulk.store.enums.EnumRol;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
@Entity
@Getter
@Setter
public class Empleado extends Entidad implements Serializable  {

	private static final long serialVersionUID = 1L;
		

	@NotNull
    @Size(min = 1, max = 50, message = "El nombre debe medir entre 5 y 50 caracteres")
    private String nombre;
	
	@NotNull
    @Size(min = 1, max = 50, message = "La descripcion debe medir entre 5 y 250 caracteres")
    private String apellido;
	
    
    @NotNull
    @Size(min = 1, max = 50, message = "El alias debe medir entre 5 y 50 caracteres")
    private String alias;
    
    @NotNull
    @Column(name = "clave", nullable = false)
    private String clave;
    
    
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "empleado_rol", joinColumns = @JoinColumn(name = "id_empleado"), inverseJoinColumns = @JoinColumn(name = "id_rol"))
	private List<Rol> roles;
    
    @OneToMany
    private List<Compra> compras;
    
    
    public Empleado() {
    	super();
    }


	public Empleado(String nombre, String apellido, String alias, String clave, String estado, Date fechaAlta) {
		super(estado, fechaAlta);
		setAll(nombre, apellido, alias, clave, estado, fechaAlta);
	}


	
	public String rolesString() {
		StringBuilder rolesBuilder = new StringBuilder("");
		if(this.roles != null) {
			for(int i = 0; i < this.roles.size(); i++) {
				Rol r = this.roles.get(i);
				rolesBuilder.append(r.getNombre());
				if(i != this.getRoles().size() - 1) {
					rolesBuilder.append(" | ");
				}
			}
		}
		return rolesBuilder.toString();
	}


	public void setAll(String nombre, String apellido, String alias, String clave, String estado, Date fechaAlta) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.alias = alias;
		this.clave = clave;
		this.estado = estado;
		this.fechaAlta = fechaAlta;
	}


	public boolean isAdmin() {
		if(roles != null) {
			for(Rol r: this.roles) {
				if(r.getNombre().equals(EnumRol.ADMIN.name())) {
					return true;
				}
			}
		}
		return false;
	}
	
}
