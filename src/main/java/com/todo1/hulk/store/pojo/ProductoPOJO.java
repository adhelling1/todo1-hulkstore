package com.todo1.hulk.store.pojo;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
@Getter
@Setter
public class ProductoPOJO {

	private long id;
	private String estado;
	private Date fechaAlta;
	private String nombre;
	private String descripcion;
	private String codigo;
	private float precio;
	private int cantidad;
	
	public ProductoPOJO(String nombre, String descripcion, String codigo, float precio, int cantidad) {
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.codigo = codigo;
		this.precio = precio;
		this.cantidad = cantidad;
	}

}
