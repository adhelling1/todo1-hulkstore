package com.todo1.hulk.store.service.compra;

import com.todo1.hulk.store.exception.HulkStoreException;
import com.todo1.hulk.store.model.Compra;
import com.todo1.hulk.store.pojo.ProductoCompraPOJO;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
public interface ICompraService extends ICompraFindService, ICompraUpdateService{

	public void agregarProducto(Compra compra, ProductoCompraPOJO productoCompraBean) throws HulkStoreException;
	public double getPrecioCompra(Compra compra);
	public void elimiarProducto(Compra compra, int index);

}
