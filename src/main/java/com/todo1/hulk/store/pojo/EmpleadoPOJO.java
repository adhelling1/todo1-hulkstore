package com.todo1.hulk.store.pojo;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
@Getter
@Setter
public class EmpleadoPOJO implements Serializable {

	private static final long serialVersionUID = -6372739133336612963L;

	private String nombre;
	private String apellido;
	private String alias;
	private String clave;
	private boolean admin;

}
