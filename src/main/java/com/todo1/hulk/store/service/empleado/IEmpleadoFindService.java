package com.todo1.hulk.store.service.empleado;

import java.util.List;

import com.todo1.hulk.store.model.Compra;
import com.todo1.hulk.store.model.Empleado;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
public interface IEmpleadoFindService {
	
	public List<Empleado> getAll();
	public Empleado findById(long id);
	public List<Empleado> findByEstado(String estado);
	public Empleado findByNombre(String nombre);
	public Empleado findByAlias(String alias);
	public List<Compra> getComprasActivas();

}
