package com.todo1.hulk.store.validator.impl;

import java.util.ArrayList;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.todo1.hulk.store.error.ErrorCode;
import com.todo1.hulk.store.exception.HulkStoreException;
import com.todo1.hulk.store.model.Compra;
import com.todo1.hulk.store.model.Entidad;
import com.todo1.hulk.store.validator.IModelValidator;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
@Component("compraValidator")
public class CompraValidatorImpl implements IModelValidator {
	
	private static Logger logger = LoggerFactory.getLogger(CompraValidatorImpl.class);

	@Override
	public Collection<String> validate(Entidad entidad) throws HulkStoreException {
		logger.info("Validando entidad {}", entidad.getClass());
		Compra compra = (Compra) entidad;
		Collection<String> errors = new ArrayList<>();
		
		if(!StringUtils.hasText(compra.getNombreCompra())) {
			errors.add(ErrorCode.NOMBRE_COMPRA_OBLIGATORIO);
		}
		
		return errors;
	}

}
