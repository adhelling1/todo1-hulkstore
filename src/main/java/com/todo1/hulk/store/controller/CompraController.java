package com.todo1.hulk.store.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.todo1.hulk.store.enums.EnumEntityType;
import com.todo1.hulk.store.enums.EnumEstado;
import com.todo1.hulk.store.exception.HulkStoreException;
import com.todo1.hulk.store.factory.EntityFactory;
import com.todo1.hulk.store.model.Compra;
import com.todo1.hulk.store.pojo.ProductoCompraPOJO;
import com.todo1.hulk.store.service.compra.ICompraService;
import com.todo1.hulk.store.service.empleado.IEmpleadoService;
import com.todo1.hulk.store.service.producto.IProductoService;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
@Controller
public class CompraController {
	
	private static final String COMPRA = "compra";
	private static final String COMPRAS = "compras";
	private static final String NUEVA_COMPRA = "nuevaCompra";


	private static Logger logger = LoggerFactory.getLogger(CompraController.class);


	@Autowired
	private ICompraService serviceCompra;
	
	@Autowired
	private IEmpleadoService empleadoService;
	
	@Autowired
	private IProductoService productosService;
	
	
	@GetMapping("/compras")
	public String compras(Model model, HttpServletRequest request) {
		logger.info("Accediendo a controlador /compras");
		HttpSession session= request.getSession();
		session.setAttribute(COMPRA, null);
		List<Compra> compras = empleadoService.getComprasActivas();
		model.addAttribute(COMPRAS, compras);
		return COMPRAS;
	}
	
	@GetMapping(path = "/borrarCompra/{id}")
	public String borrarCompra(@PathVariable(name = "id") long id) throws HulkStoreException {
		logger.info("Accediendo a controlador /borrarCompra/{id}");
		serviceCompra.delete(id);
		return "redirect:/" + COMPRAS;
	}
	
	@GetMapping(path = "/editarCompra/{id}")
	public String editarCompra(@PathVariable(name = "id") long id) {
		logger.info("Accediendo a controlador /editarCompra/{id}");
		return "editarCompra";
	}
	
	@GetMapping("/nuevaCompra")
	public String nuevaCompra(Model model, RedirectAttributes atributos, HttpServletRequest request) {
		logger.info("Accediendo a controlador /nuevaCompra");
		
		HttpSession session= request.getSession();
		Compra compra = (Compra) session.getAttribute(COMPRA);
		if(compra == null) {
			compra = (Compra) EntityFactory.getInstance().getEntidad((EnumEntityType.COMPRA));
			compra.setEstado(EnumEstado.BAJA.name()); //por defecto nace como baja, se activa al crearlo
			compra.setEmpleado(empleadoService.getEmpleadoLogueado());
			compra.setCompraProductos(new ArrayList<>());
		}
		
		atributos.addFlashAttribute("persona", compra);
		
		compra.setPrecioTotal(serviceCompra.getPrecioCompra(compra));
		
		
		//Envio la nueva compra
		model.addAttribute(COMPRA, compra);

		//guardo la compra en session
		session.setAttribute(COMPRA, compra);
		
		return NUEVA_COMPRA;
	}
	
	
	@GetMapping("/agregarCompraProd")
	public String agregarCompraProd(Model model, HttpServletRequest request) {
		logger.info("Accediendo a controlador /agregarCompraProd");
		model.addAttribute("productoCompra", new ProductoCompraPOJO());
		HttpSession session= request.getSession();
		Compra compra = (Compra) session.getAttribute(COMPRA);
		model.addAttribute("productos", productosService.findByValidos(compra));
		
		return "agregarProductoCompra";
	}
	
	@PostMapping("/agregarProdCompra")
	public RedirectView agregarProdCompra(@ModelAttribute("productoCompraBean") ProductoCompraPOJO productoCompraBean, BindingResult result, RedirectAttributes redirectAttributes, HttpServletRequest request) throws HulkStoreException {
		logger.info("Accediendo a controlador /agregarProdCompra");
		final RedirectView redirectView = new RedirectView(NUEVA_COMPRA, true);

		HttpSession session= request.getSession();
		Compra compra = (Compra) session.getAttribute(COMPRA);
		serviceCompra.agregarProducto(compra, productoCompraBean);
		
		return redirectView;
	}
	
	@GetMapping(path = "/eliminarCompraProd/{index}")
	public String eliminarCompraProd(@PathVariable(name = "index") int index, HttpServletRequest request) {
		logger.info("Accediendo a controlador /eliminarCompraProd/{index}");
		HttpSession session= request.getSession();
		Compra compra = (Compra) session.getAttribute(COMPRA);
		
		serviceCompra.elimiarProducto(compra, index);
		return "redirect:/nuevaCompra";
	}
	
	@GetMapping(path = "/editarCompraProd/{index}")
	public String editarCompraProd(@PathVariable(name = "index") int index) {
		logger.info("Accediendo a controlador /editarCompraProd/{index}");
		return "editarProdCompra";
	}
	
	
	@PostMapping("/guardarCompra")
	public RedirectView guardarCompra(@ModelAttribute(COMPRA) Compra compra, BindingResult result, HttpServletRequest request) throws HulkStoreException {
		logger.info("Accediendo a controlador /guardarCompra");
		final RedirectView redirectView = new RedirectView(COMPRAS, true);
		if(result.hasErrors()) {
			redirectView.setUrl("guardarCompra");
		}else {	
			HttpSession session= request.getSession();
			Compra compraGuardada = (Compra) session.getAttribute(COMPRA);
			compraGuardada.setNombreCompra(compra.getNombreCompra());
			serviceCompra.guardar(compraGuardada);
			session.setAttribute(COMPRA, null);
		}
		return redirectView;
		
	}


}