package com.todo1.hulk.store;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;

import com.todo1.hulk.store.enums.EnumEntityType;
import com.todo1.hulk.store.enums.EnumEstado;
import com.todo1.hulk.store.enums.EnumRol;
import com.todo1.hulk.store.exception.HulkStoreException;
import com.todo1.hulk.store.factory.EntityFactory;
import com.todo1.hulk.store.model.Compra;
import com.todo1.hulk.store.model.CompraProducto;
import com.todo1.hulk.store.model.Empleado;
import com.todo1.hulk.store.model.Producto;
import com.todo1.hulk.store.model.Rol;
import com.todo1.hulk.store.pojo.ProductoPOJO;
import com.todo1.hulk.store.service.compra.ICompraService;
import com.todo1.hulk.store.service.empleado.IEmpleadoService;
import com.todo1.hulk.store.service.producto.IProductoService;
import com.todo1.hulk.store.service.rol.IRolService;

@SpringBootApplication
public class HulkStoreTodo1Application {

	private static final String COD_PROD1 = "779123556";
	private static final String COD_PROD2 = "779569875";
	private static final String COD_PROD3 =  "779123652";
	private static final String ADMIN = "admin";
	private static final String USER = "user";
	
	private static Logger logger = LoggerFactory.getLogger(HulkStoreTodo1Application.class);

	public static void main(String[] args) {
		SpringApplication.run(HulkStoreTodo1Application.class, args);
	}

	/**
	 * Con esto permito usar isAuthenticated en el menu. Depende de la version de spring boot usada
	 * 
	 * @return
	 */
	@Bean
	public SpringSecurityDialect securityDialect() {
		return new SpringSecurityDialect();
	}

	/**
	 * Si no existen los roles los creo
	 */
	@Bean
	CommandLineRunner initDataBaseRol(IRolService service) {
		logger.info("Generando roles por defecto");
		return args -> {
			service.guardar(createRol(EnumRol.ADMIN.name()));
			service.guardar(createRol(EnumRol.USER.name()));
		};
	}
	
	private Rol createRol(String name) {
		Rol rol = (Rol) EntityFactory.getInstance().getEntidad((EnumEntityType.ROL));
		rol.setNombre(name);
		return rol;
	}

	/**
	 * Creo operadores iniciales
	 * 
	 * @param service
	 * @return
	 */
	@Bean
	CommandLineRunner initDataBaseEmpleado(IEmpleadoService service) {
		logger.info("Creando operadores 2 por defecto");
		return args -> {
			service.guardar(getEmpleado("Alejandro", "Helling", ADMIN, "123", EnumEstado.ACTIVO.name(), new Date()), true);
			service.guardar(getEmpleado("Ana", "Andolfi", USER, "123", EnumEstado.ACTIVO.name(), new Date()), false);
		};
	}
	
	private Empleado getEmpleado(String nombre, String apellido, String alias, String clave, String estado, Date fechaAlta) {
		Empleado empleado = (Empleado)EntityFactory.getInstance().getEntidad((EnumEntityType.EMPLEADO));
		empleado.setAll(nombre, apellido, alias, clave, estado, fechaAlta);
		return empleado;
	}

	/**
	 * Creo Productos iniciales
	 * 
	 * @param service
	 * @return
	 */
	@Bean
	CommandLineRunner initDataBaseProducto(IProductoService service) {
		logger.info("Creando 3 productos por defecto");
		return args -> {
			ProductoPOJO p1 = getProducto("producto 1", "la descripcion de p1", COD_PROD1, 10.5F, 100);
			ProductoPOJO p2 = getProducto("producto 2", "la descripcion de p2", COD_PROD2, 15.5F, 200);
			ProductoPOJO p3 = getProducto("producto 3", "la descripcion de p3", COD_PROD3, 20.5F, 150);
			try {
				service.guardar(p1);
				service.guardar(p2);
				service.guardar(p3);
			} catch (HulkStoreException e) {
				logger.error("Error creando 3 productos por defecto: {}", e.getMessage());
				e.printStackTrace();
			}
		};
	}
	
	private ProductoPOJO getProducto(String nombre, String descripcion, String codigo, float precio, int cantidad) {
		return new ProductoPOJO(nombre, descripcion, codigo, precio, cantidad);
	}

	/**
	 * Creo compras iniciales
	 * 
	 * @param service
	 * @param prodService
	 * @param emplService
	 * @return
	 */
	@Bean
	CommandLineRunner initDataBaseCompra(ICompraService service, IProductoService prodService, IEmpleadoService emplService) {
		logger.info("Creando 2 compra por defecto");
		return args -> {
			Producto p1 = prodService.findByCodigo(COD_PROD1);
			Producto p2 = prodService.findByCodigo(COD_PROD2);
			Empleado empleado = emplService.findByAlias(ADMIN);

			crearCompra(service, "Compra 1", empleado, p1, p2, 1, 2);
			crearCompra(service, "Compra 2", empleado, p1, p2, 3, 5);

		};
	}
	

	private void crearCompra(ICompraService service, String nombre, Empleado empleado, Producto p1, Producto p2, int cantP1, int cantP2) {
		try {
			Compra compra = (Compra) EntityFactory.getInstance().getEntidad((EnumEntityType.COMPRA));
			compra.setNombreCompra(nombre);
			compra.setFechaAlta(new Date());
			compra.setEmpleado(empleado);
			Collection<CompraProducto> compraProductos = new ArrayList<>();
			CompraProducto cp1 = new CompraProducto(cantP1, p1, compra);
			CompraProducto cp2 = new CompraProducto(cantP2, p2, compra);
			compraProductos.add(cp1);
			compraProductos.add(cp2);
			compra.setCompraProductos(compraProductos);

			service.guardar(compra);
		} catch (HulkStoreException e) {
			e.printStackTrace();
			logger.error("Error creando compra por defecto: {}", e.getMessage());
		}

	}

}
