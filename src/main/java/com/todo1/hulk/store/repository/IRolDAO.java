package com.todo1.hulk.store.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.todo1.hulk.store.model.Rol;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
@Repository
public interface IRolDAO  extends JpaRepository<Rol, Long>{ 
	public Rol findByNombre(String nombre);

}
