package com.todo1.hulk.store.validator;

import java.util.Collection;

import com.todo1.hulk.store.exception.HulkStoreException;
import com.todo1.hulk.store.model.Entidad;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 * Validaciones particulares que no se realicen mediante anotaciones
 */

public interface IModelValidator {

	public Collection<String> validate(Entidad entidad) throws HulkStoreException;
	
	//TODO: completar validaciones de todos los hijos

}
