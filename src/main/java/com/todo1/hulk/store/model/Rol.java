package com.todo1.hulk.store.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.todo1.hulk.store.enums.EnumEstado;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 */
@Entity
@Table
@Getter
@Setter
public class Rol extends Entidad{
	

	private static final long serialVersionUID = 209219182736060980L;
	

	@Column(name = "nombre",length = 20, nullable = false)
	private String nombre;
	
	@ManyToMany(cascade = CascadeType.ALL, mappedBy = "roles")
    private List<Empleado> empleadosRoles;

	public Rol() {
		super(EnumEstado.ACTIVO.name(), new Date());
	}
	
	public Rol(String nombre) {
		this();
		this.nombre = nombre;
	}
	
	
}