package com.todo1.hulk.store.pojo;

import java.io.Serializable;

import com.todo1.hulk.store.model.Empleado;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmpleadoModificacionPOJO implements Serializable {

	private static final long serialVersionUID = -6372739133336612963L;

	private Empleado empleado;

	private boolean admin;

}
