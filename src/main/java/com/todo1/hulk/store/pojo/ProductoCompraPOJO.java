package com.todo1.hulk.store.pojo;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
@Setter
@Getter
public class ProductoCompraPOJO {

	private long idProducto;
	private int cantidad;
}
