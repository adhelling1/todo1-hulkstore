package com.todo1.hulk.store.service.rol;

import com.todo1.hulk.store.exception.HulkStoreException;
import com.todo1.hulk.store.model.Rol;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
public interface IRolService extends IRolFindService{

	public Rol guardar(Rol rol) throws HulkStoreException;

}
