package com.todo1.hulk.store.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.todo1.hulk.store.model.Compra;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
@Repository
public interface ICompraDAO extends JpaRepository<Compra, Long>{

	List<Compra> findByEstado(String estado); 
	
	

}
