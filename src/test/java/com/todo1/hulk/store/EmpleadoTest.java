package com.todo1.hulk.store;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.todo1.hulk.store.enums.EnumEntityType;
import com.todo1.hulk.store.enums.EnumEstado;
import com.todo1.hulk.store.exception.HulkStoreException;
import com.todo1.hulk.store.factory.EntityFactory;
import com.todo1.hulk.store.model.Empleado;
import com.todo1.hulk.store.service.empleado.IEmpleadoService;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
@SpringBootTest
public class EmpleadoTest {
	
	@Autowired
	private IEmpleadoService service;
	
	private static Logger logger = LoggerFactory.getLogger(EmpleadoTest.class);
	
	
	
	@Test
	public void crearEmpleadoAdmin() {
		logger.info("Arrancando test crearEmpleadoAdmin");
		try {
			createEmpleado("Alejandro", "Helling", "admin1", "123", true);
		} catch (HulkStoreException e) {
			e.printStackTrace();
			logger.info("HulkStoreException en test crearEmpleadoAdmin:  {} ", e.getMessage());
			assertTrue(false);
		}
		logger.info("Finalizando test crearEmpleadoAdmin");
	}
	
	@Test
	public void crearEmpleadoUser(){
		logger.info("Arrancando test crearEmpleadoUser");
		try {
			createEmpleado("Daniel", "Helling", "user1", "123", false);
		} catch (HulkStoreException e) {
			e.printStackTrace();
			logger.info("HulkStoreException en test crearEmpleadoUser: {} ", e.getMessage());
			assertTrue(false);
		}
		logger.info("Finalizando test crearEmpleadoUser");
	}
	
	private Empleado createEmpleado(String nombre, String apellido, String alias, String clave, boolean admin) throws HulkStoreException {
		int count = service.getAll().size();
		Empleado empl =   (Empleado) EntityFactory.getInstance().getEntidad((EnumEntityType.EMPLEADO));
		empl.setAll(nombre, apellido, alias, clave, EnumEstado.ACTIVO.name(), new Date());
		Empleado nuevo = service.guardar(empl, admin);
		count++;
		assertTrue(service.getAll().size() == count);
		return nuevo;
	}
	
	@Test
	public void deleteEmpleado(){
		logger.info("Arrancando test deleteEmpleado");
		List<Empleado> empleados = service.getAll();
		if(empleados.isEmpty()) {
			crearEmpleadoAdmin();
		}
		Empleado empl = service.getAll().get(0);
		try {
			service.delete(empl.getId());
			assertTrue(service.findById(empl.getId()).getEstado().equals(EnumEstado.BAJA.name()));
			
			//vuelvo a dejarlo activo
			empl.setEstado(EnumEstado.ACTIVO.name());
			service.modificar(empl);
			assertTrue(service.findById(empl.getId()).getEstado().equals(EnumEstado.ACTIVO.name()));
		} catch (HulkStoreException e) {
			e.printStackTrace();
			logger.info("HulkStoreException en test deleteEmpleado: {} ", e.getMessage());
			assertTrue(false);
		}
		
	
		logger.info("Finalizando test deleteEmpleado");
	}

}
