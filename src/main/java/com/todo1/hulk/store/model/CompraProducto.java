package com.todo1.hulk.store.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
@Entity
@Getter
@Setter
public class CompraProducto implements Serializable {

	private static final long serialVersionUID = -2376658582772177002L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@NotNull
	@Min(value = 0, message = "La cantidad mínima es 0")
	private int cantidad;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_producto", nullable = false)
	private Producto producto;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_compra", nullable = false)
	private Compra compra;

	public CompraProducto() {

	}

	public CompraProducto(int cantidad, Producto producto, Compra compra) {
		super();
		this.cantidad = cantidad;
		this.producto = producto;
		this.compra = compra;
	}

}
