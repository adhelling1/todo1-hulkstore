package com.todo1.hulk.store.validator;

import com.todo1.hulk.store.model.Producto;
import com.todo1.hulk.store.pojo.ProductoPOJO;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
public interface IProductoUpdateValidator {
	
	public boolean hasChanges(ProductoPOJO prod, Producto prodOriginal);


}
