package com.todo1.hulk.store.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.todo1.hulk.store.model.Empleado;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
@Repository
public interface IEmpleadoDAO extends JpaRepository<Empleado, Long>{ 
	
	public Empleado findByAlias(String alias);
	public Empleado findByNombre(String nombre);
	public Empleado findByApellido(String alias);
	public List<Empleado> findByEstado(String estado);

}
