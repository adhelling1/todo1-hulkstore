package com.todo1.hulk.store.validator.impl;

import java.util.ArrayList;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.todo1.hulk.store.error.ErrorCode;
import com.todo1.hulk.store.exception.HulkStoreException;
import com.todo1.hulk.store.model.Entidad;
import com.todo1.hulk.store.model.Rol;
import com.todo1.hulk.store.validator.IModelValidator;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
@Component("rolValidator")
public class RolValidator  implements IModelValidator {
	
	private static Logger logger = LoggerFactory.getLogger(RolValidator.class);

	@Override
	public Collection<String> validate(Entidad entidad) throws HulkStoreException {
		logger.info("Validando entidad {}", entidad.getClass());
		Rol rol = (Rol) entidad;
		Collection<String> errors = new ArrayList<>();
		
		if(!StringUtils.hasText(rol.getNombre())) {
			errors.add(ErrorCode.NOMBRE_ROL_OBLIGATORIO);
		}

		return errors;
	}

	
}
