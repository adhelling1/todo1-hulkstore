package com.todo1.hulk.store.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.todo1.hulk.store.model.Producto;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
@Repository
public interface IProductoDAO extends JpaRepository<Producto, Long>{ 
	
	public Producto findByCodigo(String codigo);
	public Producto findByNombre(String codigo);
	public List<Producto> findByEstado(String estado);
	
}
