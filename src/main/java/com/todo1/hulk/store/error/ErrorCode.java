package com.todo1.hulk.store.error;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
public class ErrorCode {
	
	private ErrorCode() {
		
	}

	public static final String NOMBRE_COMPRA_OBLIGATORIO = "NOMBRE_OBLIGATORIO";
	public static final String NOMBRE_PRODUCTO_OBLIGATORIO = "NOMBRE_PRODUCTO_OBLIGATORIO";
	public static final String NOMBRE_ROL_OBLIGATORIO = "NOMBRE_ROL_OBLIGATORIO";
	public static final String NOMBRE_EMPLEADO_OBLIGATORIO = "NOMBRE_EMPLEADO_OBLIGATORIO";
	public static final String CODIGO_REPETIDO = "CODIGO_REPETIDO";

}
