package com.todo1.hulk.store.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.Size;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
@MappedSuperclass
@Getter
@Setter
public class Entidad implements Serializable {

	private static final long serialVersionUID = 1551368426113744610L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
	
	@NotNull
	@Size(min = 1, max = 20, message = "El estado debe contener entre 1 y 20 caracteres")
	@Column(name = "estado", length = 20, nullable = false)
	protected String estado;
	
	@NotNull
	protected Date fechaAlta = new Date();
	
	public Entidad() {
		
	}

	public Entidad(String estado, Date fechaAlta) {
		super();
		this.estado = estado;
		this.fechaAlta = fechaAlta;
	}


}
