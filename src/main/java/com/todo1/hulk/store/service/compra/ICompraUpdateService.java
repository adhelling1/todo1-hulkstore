package com.todo1.hulk.store.service.compra;

import com.todo1.hulk.store.exception.HulkStoreException;
import com.todo1.hulk.store.model.Compra;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
public interface ICompraUpdateService {
	
	public boolean delete(long id) throws HulkStoreException;
	public Compra guardar(Compra compra) throws HulkStoreException;

}
