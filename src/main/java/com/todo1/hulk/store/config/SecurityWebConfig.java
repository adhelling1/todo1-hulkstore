package com.todo1.hulk.store.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.todo1.hulk.store.service.impl.EmpleadoServiceImpl;

/***
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */

@Configuration
@EnableWebSecurity
@Order(1)
public class SecurityWebConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private BCryptPasswordEncoder bCrypt;

	@Autowired
	private EmpleadoServiceImpl empleadoService;


	private static Logger logger = LoggerFactory.getLogger(SecurityWebConfig.class);

	// Necesario para evitar que la seguridad se aplique a los resources
	// Como los css, imagenes y javascripts
	String[] resources = new String[] { "/include/**", "/css/**", "/icons/**", "/img/**", "/js/**", "/layer/**" };

	@Bean
	public BCryptPasswordEncoder getPasswordEncoder() {
		logger.info("creando BCryptPasswordEncoder");
		// El numero 4 representa que tan fuerte quieres la encriptacion. Se puede en un rango entre 4 y 31.
		return new BCryptPasswordEncoder(4);
	}

	@Override
	protected void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
		logger.info("Configurando clase enccargada de la autenticacion");
		authenticationManagerBuilder.userDetailsService(empleadoService).passwordEncoder(bCrypt);
	}

	// Registra el service para usuarios y el encriptador de contrasena
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(empleadoService).passwordEncoder(getPasswordEncoder());
	}

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		logger.info("Configuranco permisos sobre recursos");

		httpSecurity
          .authorizeRequests()
	        .antMatchers(resources).permitAll()  
	        .antMatchers("/","/login").permitAll()
	        .antMatchers("/admin*").permitAll()
	        .antMatchers("/user*").permitAll()
              .anyRequest().authenticated()
              .and()
          .formLogin()
              .loginPage("/login")
              .permitAll()
              .defaultSuccessUrl("/menu")
              .failureUrl("/login?error=true")
              .usernameParameter("nombre")
              .passwordParameter("clave")
              .and()
          .logout()
              .permitAll()
              .logoutSuccessUrl("/login?logout");
	}

}
