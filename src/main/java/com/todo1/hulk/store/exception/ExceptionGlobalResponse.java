package com.todo1.hulk.store.exception;

import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 * Excepcion general para los controladores, para los casos no tratados
 */
@ControllerAdvice
public class ExceptionGlobalResponse {
	
	private static Logger logger = LoggerFactory.getLogger(ExceptionGlobalResponse.class);
	

	@ExceptionHandler(RuntimeException.class)
	public ResponseEntity<Response> runtimeException(RuntimeException e) {
		logger.error("Capturando RuntimeException:{} ", e.getMessage());
		return createResult(e);
	}
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<Response> exception(Exception e) {
		logger.error("Capturando Exception: {}", e.getMessage());
		return createResult(e);
	}
	
	@ExceptionHandler(HulkStoreException.class)
	public ResponseEntity<Response> hulkStoreException(Exception e) {
		logger.error("Capturando hulkStoreException: {}", e.getMessage());
		return createResult(e);
	}
	
	private ResponseEntity<Response> createResult(Exception e) {
		Response result = new Response(getTime(), "[Exception Response] - Exception: " + e.getMessage(), 500, "Error");
		return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	
	
	public static Timestamp getTime() {
		return new Timestamp(System.currentTimeMillis());
	}
}