package com.todo1.hulk.store.service.empleado;

import com.todo1.hulk.store.exception.HulkStoreException;
import com.todo1.hulk.store.model.Empleado;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
public interface IEmpleadoUpdateService {
	
	public boolean delete(long id) throws HulkStoreException;
	public Empleado guardar(Empleado empleado, boolean admin) throws HulkStoreException;
	public Empleado modificar(Empleado empleado) throws HulkStoreException;
	public Empleado modificar(Empleado empleado, boolean admin) throws HulkStoreException;

}
