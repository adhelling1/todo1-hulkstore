package com.todo1.hulk.store.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.todo1.hulk.store.enums.EnumEstado;
import com.todo1.hulk.store.enums.EnumRol;
import com.todo1.hulk.store.exception.HulkStoreException;
import com.todo1.hulk.store.model.Compra;
import com.todo1.hulk.store.model.Empleado;
import com.todo1.hulk.store.model.Rol;
import com.todo1.hulk.store.repository.IEmpleadoDAO;
import com.todo1.hulk.store.repository.IRolDAO;
import com.todo1.hulk.store.service.empleado.IEmpleadoService;
import com.todo1.hulk.store.validator.IModelValidator;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
@Service
@Transactional
public class EmpleadoServiceImpl implements IEmpleadoService {

	@Autowired
	private IEmpleadoDAO repoEmpleadoDao;

	@Autowired
	private IRolDAO repoRolDao;

	@Autowired
	private BCryptPasswordEncoder encoder;
	
	@Autowired
	@Qualifier("empleadoValidator")
	private IModelValidator empleadoValidator;


	private static Logger logger = LoggerFactory.getLogger(EmpleadoServiceImpl.class);

	/**
	 * Obtengo la lista de empleados
	 */
	@Override
	public List<Empleado> getAll() {
		logger.info("Obteniendo todas las compras");
		return repoEmpleadoDao.findAll();
	}

	/**
	 * Busco empleado por identificador
	 */
	@Override
	public Empleado findById(long id) {
		logger.info("Obteniendo empleado de id {}", id);
		Optional<Empleado> empleado = repoEmpleadoDao.findById(id);
		if (empleado.isPresent()) {
			return empleado.get();
		}
		return null;
	}

	/**
	 * Elimino elmpelado por identificador
	 */
	@Override
	public boolean delete(long id) throws HulkStoreException {
		logger.info("borrando empleado de id: {}", id);
		Optional<Empleado> empleado = repoEmpleadoDao.findById(id);
		if (empleado.isPresent()) {
			Empleado empleadoLogeado = getEmpleadoLogueado();
			if (empleadoLogeado != null && empleadoLogeado.getId() == empleado.get().getId()) {
				throw new HulkStoreException("no se puede elimair el usuario logueado");
			}
			empleado.get().setEstado(EnumEstado.BAJA.name());
			Empleado e = repoEmpleadoDao.save(empleado.get());
			return e != null;
		}
		return false;
	}

	/**
	 * Busco empleado por nombre
	 */
	@Override
	public Empleado findByNombre(String nombre) {
		logger.info("Obteniendo empleado de nombre: {}", nombre);
		return repoEmpleadoDao.findByNombre(nombre);
	}

	/**
	 * Busco un empleado por alias
	 */
	@Override
	public Empleado findByAlias(String alias) {
		logger.info("Obteniendo empleado de alias: {}", alias);
		return repoEmpleadoDao.findByAlias(alias);
	}

	/**
	 * Busco empleado por estado
	 */
	@Override
	public List<Empleado> findByEstado(String estado) {
		logger.info("Obteniendo empleado con estado: {}", estado);
		return repoEmpleadoDao.findByEstado(estado);
	}

	/**
	 * Almaceno un empleado y le doy los roles dependiendo variable de entrada
	 */
	@Override
	public Empleado guardar(Empleado empleado, boolean admin) throws HulkStoreException {
		logger.info("Almacenando empleado de tipo: {}", ((admin)? EnumRol.ADMIN.name() : EnumRol.USER.name()));
		validarEmpleadoExistente(empleado);

		empleado.setClave(encoder.encode(empleado.getClave()));
		empleado.setEstado(EnumEstado.ACTIVO.name());

		List<Rol> roles = getRoles(admin);
		empleado.setRoles(roles);
		
		return validateSave(empleado);
	}
	
	/**
	 * Dependiendo si es administrador o no, devuelve la lista de roles
	 * @param admin
	 * @return
	 */
	private List<Rol> getRoles(boolean admin) {
		List<Rol> roles = new ArrayList<>();
		Rol rolUser = getRol(EnumRol.USER.name());
		if (rolUser != null) {
			roles.add(rolUser);
		}

		// un administrador, es administrador y usuario
		if (admin) {
			Rol rolAdmin = getRol(EnumRol.ADMIN.name());
			if (rolAdmin != null) {
				roles.add(rolAdmin);
			}
		}
		return roles;
	}

	/**
	 * Valida y guarda la entidad
	 * @param empleado
	 * @return
	 * @throws HulkStoreException
	 */
	protected Empleado validateSave(Empleado empleado) throws HulkStoreException {
		Collection<String> errors = empleadoValidator.validate(empleado);
		if(errors == null || errors.isEmpty()) {
			return repoEmpleadoDao.saveAndFlush(empleado);
		}else {
			throw new HulkStoreException(errors.toString());
		}
	}
	
	

	/**
	 * Busco el rol por nombre
	 * 
	 * @return
	 */
	private Rol getRol(String nombre) {
		return repoRolDao.findByNombre(nombre);
	}

	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		logger.info("Obteneiendo UserDetails de empleado logueado");
		Empleado empleado = repoEmpleadoDao.findByAlias(username);
		Collection<SimpleGrantedAuthority> roles = getRoles(empleado);
		return new User(empleado.getAlias(), empleado.getClave(), roles);
	}

	private Collection<SimpleGrantedAuthority> getRoles(Empleado empleado) {
		logger.info("Obteneiendo Authority de empleado logueado");
		Collection<SimpleGrantedAuthority> roles = new ArrayList<>();
		if (empleado.getRoles() != null) {
			empleado.getRoles().forEach(rol -> roles.add(new SimpleGrantedAuthority(rol.getNombre())));
			
		}
		return roles;
	}

	
	/**
	 * Modifico un empleado
	 */
	@Override
	public Empleado modificar(Empleado empleado) throws HulkStoreException {
		logger.info("modificando empleado");
		validarEmpleadoExistente(empleado);
		Collection<String> errors = empleadoValidator.validate(empleado);
		if(errors == null || errors.isEmpty()) {
			return repoEmpleadoDao.save(empleado);
		}else {
			throw new HulkStoreException(errors.toString());
		}
	}
	
	
	/**
	 * Valida si un empleado existe por alias
	 * @param empleado
	 * @throws HulkStoreException
	 */
	private void validarEmpleadoExistente(Empleado empleado) throws HulkStoreException {
		Empleado empleadoAlias = findByAlias(empleado.getAlias());
		if(empleadoAlias != null && (empleado.getId() == 0 || empleado.getId() != empleadoAlias.getId())) {
			throw new HulkStoreException("alias existente");
		}
		
	}

	/**
	 * Modifico un empleado y seteo roles
	 */
	@Override
	public Empleado modificar(Empleado empleado, boolean admin) throws HulkStoreException {
		logger.info("modificando empleado");
		validarEmpleadoExistente(empleado);
		Empleado original = repoEmpleadoDao.getById(empleado.getId());
				
		empleado.setCompras(original.getCompras());
		empleado.setRoles(getRoles(admin));
		Collection<String> errors = empleadoValidator.validate(empleado);
		if(errors == null || errors.isEmpty()) {
			return repoEmpleadoDao.save(empleado);
		}else {
			throw new HulkStoreException(errors.toString());
		}
	}

	/**
	 * Obtengo del contexto el usuario logueado
	 * 
	 * @return
	 */
	@Override
	public Empleado getEmpleadoLogueado() {
		logger.info("Obteneiendo usuario logueado");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth != null && auth.getPrincipal() != null) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			if(userDetail != null) {
				return findByAlias(userDetail.getUsername());
			}
		}
		return null;
	}

	/**
	 * Obtengo solo las compras activas del operador en sesion
	 */
	@Override
	public List<Compra> getComprasActivas() {
		Empleado eLogueado =  this.getEmpleadoLogueado();
		if(eLogueado != null) {
			List<Compra> compras = eLogueado.getCompras();
			compras = compras
				    .stream()
				    .filter(x -> x.getEstado().equals(EnumEstado.ACTIVO.name()))
				    .collect(Collectors.toList());
			return compras;
		}
		return new ArrayList<>();
	}

}
