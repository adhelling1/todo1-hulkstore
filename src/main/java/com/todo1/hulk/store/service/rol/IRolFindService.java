package com.todo1.hulk.store.service.rol;

import java.util.List;

import com.todo1.hulk.store.model.Rol;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
public interface IRolFindService {
	public List<Rol> getAll();
}
