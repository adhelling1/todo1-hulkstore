CREATE DATABASE "todo1HulkStore"
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'Spanish_Argentina.1252'
       LC_CTYPE = 'Spanish_Argentina.1252'
       CONNECTION LIMIT = -1;
ALTER DATABASE "todo1HulkStore" SET client_encoding='latin1';
