package com.todo1.hulk.store.exception;

import java.sql.Timestamp;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
@Getter
@Setter
public class Response {
	
	private Timestamp timestamp;
	private String data;
	private int respondeCode;
	private String status;
	
	public Response() {}
	
	public Response(Timestamp timestamp, String data, int respondeCode, String status) {
		this.timestamp = timestamp;
		this.data = data;
		this.respondeCode = respondeCode;
		this.status = status;
	}
	
}