package com.todo1.hulk.store.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */

@Entity
@Getter
@Setter
public class Producto extends Entidad implements Serializable  {
	
	private static final long serialVersionUID = 1L;


	@NotNull
    @Size(min = 1, max = 50, message = "El nombre debe medir entre 5 y 50 caracteres")
    private String nombre;
	
	@NotNull
    @Size(min = 1, max = 1000, message = "La descripcion debe medir entre 5 y 1000 caracteres")
    private String descripcion;

    @NotNull
    @Size(min = 1, max = 50, message = "El código debe medir entre 5 y 50 caracteres")
    private String codigo;

    @NotNull
    @Min(value = 0, message = "El precio mínimo es 0")
    private float precio;

    @NotNull
    @Min(value = 0, message = "La cantidad mínima es 0")
    private int cantidad;

    public Producto() {
    	super();
    }

	public Producto(String nombre, String descripcion, String codigo, float precio, int cantidad, String estado, Date fechaAlta) {
		super(estado, fechaAlta);
		setAll(nombre, descripcion, codigo, precio, cantidad, estado, fechaAlta);
	}

	public void setAll(String nombre, String descripcion, String codigo, float precio, int cantidad, String estado, Date fechaAlta) {
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.codigo = codigo;
		this.precio = precio;
		this.cantidad = cantidad;
		this.estado = estado;
		this.fechaAlta = fechaAlta;
	}


}
