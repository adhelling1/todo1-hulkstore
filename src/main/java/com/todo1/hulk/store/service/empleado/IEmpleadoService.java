package com.todo1.hulk.store.service.empleado;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.todo1.hulk.store.model.Empleado;

/**
 * 
 * @author ASUS-CHUNI - Alejandro Helling - adhelling@horuslogistics.com.ar
 *
 */
public interface IEmpleadoService extends IEmpleadoFindService, IEmpleadoUpdateService, UserDetailsService{

	public Empleado getEmpleadoLogueado();


}
