package com.todo1.hulk.store.service.impl;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.todo1.hulk.store.enums.EnumEntityType;
import com.todo1.hulk.store.enums.EnumEstado;
import com.todo1.hulk.store.exception.HulkStoreException;
import com.todo1.hulk.store.factory.EntityFactory;
import com.todo1.hulk.store.model.Compra;
import com.todo1.hulk.store.model.CompraProducto;
import com.todo1.hulk.store.model.Producto;
import com.todo1.hulk.store.pojo.ProductoPOJO;
import com.todo1.hulk.store.repository.IProductoDAO;
import com.todo1.hulk.store.service.producto.IProductoService;
import com.todo1.hulk.store.validator.impl.ProductoValidatorImpl;

@Service
@Transactional
public class ProductoServiceImpl implements IProductoService {

	@Autowired
	private IProductoDAO repoProductoDao;

	private static Logger logger = LoggerFactory.getLogger(ProductoServiceImpl.class);
	
	@Autowired
	@Qualifier("productoValidator")
	private ProductoValidatorImpl prodValidator;

	
	/**
	 * Busco todos los productos
	 */
	@Override
	public List<Producto> getAll() {
		logger.info("Obteniendo todas los productos");
		return repoProductoDao.findAll();
	}


	
	/**
	 * Elimino el producto por identificador
	 */
	@Override
	public boolean delete(long id) {
		logger.info("Eliminando producto de id: {}" , id);
		Optional<Producto> producto = repoProductoDao.findById(id);
		if (producto.isPresent()) {
			producto.get().setEstado(EnumEstado.BAJA.name());
			Producto p = repoProductoDao.save(producto.get());
			return p != null;
		}
		return false;
	}

	
	/**
	 * Busco el producto por nombre
	 */
	@Override
	public Producto findByNombre(String nombre) {
		logger.info("Obteniendo productos por nombre");
		return repoProductoDao.findByNombre(nombre);
	}

	
	/**
	 * Busco el producto por estado
	 */
	@Override
	public List<Producto> findByEstado(String estado) {
		logger.info("Obteniendo productos por estado");
		return repoProductoDao.findByEstado(estado);
	}

	
	/**
	 * Busco el producto por codigo
	 */
	@Override
	public Producto findByCodigo(String codigo) {
		logger.info("Obteniendo productos por codigo");
		return repoProductoDao.findByCodigo(codigo);
	}

	
	/**
	 * Almaceno el producto
	 */
	@Override
	public Producto guardar(ProductoPOJO prod) throws HulkStoreException {
		logger.info("Guardando nuevo producto");
		
		Producto producto = getProducto(prod);
		producto.setFechaAlta(new Date());
		producto.setEstado(EnumEstado.ACTIVO.name());
		
		return validateSave(producto);
	}
	
	/**
	 * Crea un producto a partir de POJO
	 * @param prod
	 * @return
	 */
	private Producto getProducto(ProductoPOJO prod) {
		logger.info("Creando producto a partir de un POJO");
		Producto producto = (Producto) EntityFactory.getInstance().getEntidad((EnumEntityType.PRODUCTO));
		producto.setAll(prod.getNombre(), prod.getDescripcion(), prod.getCodigo(), prod.getPrecio(), prod.getCantidad(), prod.getEstado(), prod.getFechaAlta());
		producto.setId(prod.getId());
		return producto;
	}
	
	@Override
	public Producto modificar(ProductoPOJO prodPojo) throws HulkStoreException {
		if (prodPojo == null || prodPojo.getId() == 0) {
			throw new HulkStoreException("Producto POJO inválido");
		}
	
		Producto prodOriginal = findById(prodPojo.getId());
		//valido si tengo cambios
		if(prodValidator.hasChanges(prodPojo, prodOriginal)) {
			//copio los datos del POJO al producto
			prodOriginal.setAll(prodPojo.getNombre(), prodPojo.getDescripcion(), prodPojo.getCodigo(), prodPojo.getPrecio(), prodPojo.getCantidad(), 
							    prodOriginal.getEstado(), prodOriginal.getFechaAlta());
			return validateSave(prodOriginal);
		}
		throw new HulkStoreException("Producto sin cambios");
	}
	
	@Override
	public Producto modificar(Producto prod) throws HulkStoreException {
		if (prod == null || prod.getId() == 0) {
			throw new HulkStoreException("Producto inválido");
		}
		return validateSave(prod);
	}
	
	
	protected Producto validateSave(Producto compra) throws HulkStoreException {
		Collection<String> errors = prodValidator.validate(compra);
		if(errors == null || errors.isEmpty()) {
			return repoProductoDao.saveAndFlush(compra);
		}else {
			throw new HulkStoreException(errors.toString());
		}
	}
	
	
	/**
	 * Filtro los productos activos, que tengan stock y que ya no esten en el carrito de compra
	 */
	@Override
	public List<Producto> findByValidos(Compra compra) {
		List<Producto> productos = findByEstado(EnumEstado.ACTIVO.name());
		productos = productos
			    .stream()
			    .filter(producto -> producto.getCantidad() > 0)
			    .filter(producto -> !contieneProd(producto, compra))
			    .collect(Collectors.toList());
		return productos;
	}

	/**
	 * Verifico si la comora ya tiene el producto
	 */
	private boolean contieneProd(Producto producto, Compra compra) {
		if(compra != null && producto != null) {
			for(CompraProducto cp : compra.getCompraProductos()) {
				if(cp.getProducto().getId() == producto.getId()) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Busco el producto por identificador
	 */
	@Override
	public Producto findById(long idProducto) {
		logger.info("Obteniendo productos por id");
		Optional<Producto> prod = repoProductoDao.findById(idProducto);
		if(prod.isPresent()) {
			return prod.get();
		}
		return null;
	}

}
